<?php
/**
 * @file
 * Enables modules and site configuration for a minimal site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function oxbow_core_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site']['mail']['#default_value'] = "admin@oxbowlabs.com";

  // Pre-populate the username
  $form['admin_account']['account']['name']['#default_value'] = "oxbow";

  // Pre-populate the email
  $form['admin_account']['account']['mail']['#default_value'] = "admin@oxbowlabs.com";

  // Pre-populate the country
  $form['server_settings']['site_default_country']['#default_value'] = "US";
}