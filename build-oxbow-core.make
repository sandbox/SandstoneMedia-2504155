api = 2
core = 7.x

;Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make

; Download the install profile and recursively build all its dependencies
projects[oxbow_core][type] = profile
projects[oxbow_core][download][type] = git
projects[oxbow_core][download][url] = git://git.drupal.org:sandbox/SandstoneMedia/2504155.git
projects[oxbow_core][download][branch] = 7.x-1.x-dev
