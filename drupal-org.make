core = 7.x
api = 2

; Set the default sub directory for all contributed projects.
defaults[projects][subdir] = contrib



; Modules

projects[bean][version] = 1.13

projects[better_exposed_filters][version] = 3.6

projects[calendar][version] = 3.5

projects[captcha][version] = 1.7

; !! DEPRECIATED !!
projects[ckeditor][version] = 1.16

; !! DEPRECIATED !!
projects[ckeditor_link][version] = 2.4

projects[ctools][version] = 1.15

;projects[date_ical][version] = 3.2

projects[date_popup_authored][version] = 1.2

projects[entity][version] = 1.9

projects[entity_modified][version] = 1.2

projects[globalredirect][version] = 1.6

projects[google_analytics][version] = 2.6

projects[jquery_update][version] = 3.0-alpha5

;projects[jquery_colorpicker][version] = 1.0-rc2

projects[libraries][version] = 2.5

projects[linkit][version] = 3.5

projects[mail_redirect][type] = module
projects[mail_redirect][version] = 2.0
; The following patch is only applicable to mail_redirect 2.0
projects[mail_redirect][patch][] = https://www.drupal.org/files/mail_redirect-install_warning-1916486-2.patch

projects[maillog][version] = 1.0-rc1

projects[mailsystem][version] = 2.35

; !! DEPRECIATED !!
projects[mandrill][version] = 2.2

projects[menu_attributes][version] = 1.1

projects[menu_block][version] = 2.8

projects[metatag][version] = 1.27

projects[pathauto][version] = 1.3

projects[pathologic][version] = 2.12

projects[redirect][version] = 1.0-rc3

projects[recaptcha][version] = 2.3

projects[redis][version] = 3.18

projects[render_cache][version] = 1.0

projects[rules][version] = 2.12

projects[save_draft][version] = 1.4

projects[search404][version] = 1.6

projects[smtp][version] = 1.7

projects[token][version] = 1.7

projects[views][version] = 3.24

projects[webform][version] = 4.23

projects[xmlsitemap][version] = 2.6



; Administration Modules

projects[addanother][version] = 2.2

projects[admin_menu][version] = 3.0-rc6

projects[admin_views][version] = 1.7

projects[instantfilter][version] = 2.0

projects[menu_admin_per_menu][version] = 1.1

projects[oxbow_core_status][type] = module
projects[oxbow_core_status][download][type] = git
projects[oxbow_core_status][download][url] = https://bitbucket.org/oxbowlabs/oxbow-core-status/get/a8ac77073de9.zip

projects[role_delegation][version] = 1.2

projects[simplified_menu_admin][version] = 1.0

projects[views_bulk_operations][version] = 3.6


; Development Modules

projects[backup_migrate][version] = 3.5

projects[coffee][version] = 2.3

projects[conditional_styles][version] = 2.2

projects[devel][version] = 1.5

projects[search_krumo][version] = 1.6

projects[diff][version] = 3.2

projects[environment_indicator][version] = 2.8

projects[features][version] = 2.11

projects[shield][version] = 1.3

projects[strongarm][version] = 2.0




; Field & Layout Modules

projects[classy_paragraphs][version] = 1.0

; NOTE: This project is currently hosted on github. Remove the
; following if / when the project is incorporated into classy_paragraphs.
; classy_paragraphs_ui is only tested against classy_paragraphs
; version 1.0-alpha4.
projects[classy_paragraphs_ui][type] = module
projects[classy_paragraphs_ui][download][type] = git
projects[classy_paragraphs_ui][download][url] = https://github.com/dervishmoose/classy_paragraphs_ui.git

projects[color_field][version] = 1.8

projects[date][version] = 2.10

; projects[ds][version] = 2.8

projects[elements][version] = 1.5

projects[email][version] = 1.3

projects[entity_view_mode][version] = 1.0-rc1

projects[entityreference][version] = 1.5

projects[extlink][version] = 1.21

; dev module in use to prevent error message on install
; https://www.drupal.org/node/2081681
;projects[fences][version] = 1.x-dev
;projects[fences][download][type] = git
;projects[fences][download][url] = http://git.drupal.org/project/fences.git
;projects[fences][revision] = 67206b5220cbebf52e8e6a290ccb977b2420b2ad

projects[field_collection][version] = 1.1

projects[field_group][version] = 1.6

projects[html5_tools][version] = 1.3

projects[inline_entity_form][version] = 1.9

projects[panels][version] = 3.10

projects[panels_everywhere][version] = 1.0

projects[paragraphs][version] = 1.0-rc5

projects[paragraphs_pack][version] = 1.0-alpha5

projects[shs][version] = 1.8

projects[smart_trim][version] = 1.6

projects[telephone][version] = 1.0-alpha1

projects[link][version] = 1.7

projects[viewfield][version] = 2.2




; Media Modules

projects[filefield_sources][version] = 1.11

projects[imagecache_actions][version] = 1.12

projects[imagefield_crop][version] = 1.1

projects[imce][version] = 1.11

projects[imce_mkdir][version] = 1.0

projects[retina_images][version] = 1.0-beta5

projects[youtube][version] = 1.6




; User Modules

projects[email_registration][version] = 1.5

projects[force_password_change][version] = 1.0-rc2

projects[genpass][version] = 1.1

projects[mass_pwreset][version] = 1.1
projects[mass_pwreset][download][type] = git
projects[mass_pwreset][download][url] = http://git.drupal.org/project/mass_pwreset.git
projects[mass_pwreset][download][revision] = 3985b74e919db497eb0bc562021c0d83d04ba4d7

projects[password_policy][version] = 2.0-alpha8
; Fixes broken registration page for 2.0-alpha5
;projects[password_policy][patch][] = https://www.drupal.org/files/issues/password_policy-7.x-2.x-fix_element_alter_error-2489918-4.patch
; *** Path no longer needed - https://www.drupal.org/node/2643448 ***

projects[prlp][version] = 1.3
; Oxbow contributed patch! Changes the access argument for PRLP config from
; "Administer Users" to "Administer PRLP"
projects[prlp][patch][] = https://www.drupal.org/files/issues/prlp-add_hook_permission-2572237-1.patch


; Libraries
libraries[bgrins-spectrum][download][type] = git
libraries[bgrins-spectrum][download][url] = https://github.com/bgrins/spectrum.git
libraries[bgrins-spectrum][directory_name] = bgrins-spectrum

libraries[colorbox][download][type] = get
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/archive/1.x.zip
libraries[colorbox][directory_name] = colorbox

libraries[htmlpurifier][download][type] = get
libraries[htmlpurifier][download][url] = http://htmlpurifier.org/releases/htmlpurifier-4.6.0.tar.gz
libraries[htmlpurifier][directory_name] = htmlpurifier

libraries[mandrill][download][type] = git
libraries[mandrill][download][url] = https://bitbucket.org/mailchimp/mandrill-api-php.git
libraries[mandrill][download][branch] = 1.0.54
libraries[mandrill][directory_name] = mandrill

libraries[plupload][download][type] = get
libraries[plupload][download][url] = https://github.com/moxiecode/plupload/archive/v1.5.8.zip
libraries[plupload][directory_name] = plupload

libraries[predis][download][type] = git
libraries[predis][download][url] = https://github.com/nrk/predis.git
libraries[predis][download][branch] = v1.0
libraries[predis][directory_name] = predis

libraries[jquery-simple-color][download][type] = get
libraries[jquery-simple-color][download][url] = https://github.com/recurser/jquery-simple-color/archive/v1.2.1.zip
libraries[jquery-simple-color][directory_name] = jquery-simple-color
